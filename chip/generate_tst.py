import random

def generate_16bit_tst():
	with open('And16.tst', 'w') as f:
		f.write('load And16.hdl,\noutput-file Add16.out,\noutput-list a b out;\n')
		for x in range(0x10):
			f.write(f'set a %B{bin(random.randint(0, 0x10000))[2:]}, set b %B{bin(random.randint(0, 0x10000))[2:]}, eval, output;')
			f.write('\n')

generate_16bit_tst()