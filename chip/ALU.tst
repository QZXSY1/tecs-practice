load ALU.hdl,
output-file ALU.out,
output-list x y zx nx zy ny f no;

set x 123, set y 321, set zx 0, set zy 0, set nx 0, set ny 0, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 1, set zy 0, set nx 0, set ny 0, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 1, set nx 0, set ny 0, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 1, set ny 0, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 0, set ny 1, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 0, set ny 0, set f 1, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 0, set ny 0, set f 0, set no 1, eval, output;

set x 123, set y 321, set zx 1, set zy 1, set nx 0, set ny 0, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 1, set nx 1, set ny 0, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 1, set ny 1, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 0, set ny 1, set f 1, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 0, set ny 0, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 0, set nx 0, set ny 0, set f 0, set no 1, eval, output;

set x 123, set y 321, set zx 1, set zy 1, set nx 1, set ny 0, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 1, set nx 1, set ny 1, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 1, set ny 1, set f 1, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 0, set ny 1, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 0, set nx 0, set ny 0, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 1, set nx 0, set ny 0, set f 0, set no 1, eval, output;

set x 123, set y 321, set zx 1, set zy 1, set nx 1, set ny 1, set f 0, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 1, set nx 1, set ny 1, set f 1, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 0, set nx 1, set ny 1, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 0, set nx 0, set ny 1, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 1, set nx 0, set ny 0, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 1, set nx 1, set ny 0, set f 0, set no 1, eval, output;

set x 123, set y 321, set zx 1, set zy 1, set nx 1, set ny 1, set f 1, set no 0, eval, output;
set x 123, set y 321, set zx 0, set zy 1, set nx 1, set ny 1, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 0, set nx 1, set ny 1, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 1, set nx 0, set ny 1, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 1, set nx 1, set ny 0, set f 1, set no 1, eval, output;
set x 123, set y 321, set zx 1, set zy 1, set nx 1, set ny 1, set f 0, set no 1, eval, output;

set x 123, set y 321, set zx 1, set zy 1, set nx 1, set ny 1, set f 1, set no 1, eval, output;